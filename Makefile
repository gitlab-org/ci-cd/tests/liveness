VERSION = 0.1.0
CI_COMMIT_SHORT_SHA ?= dev
CI_REGISTRY_IMAGE ?= liveness

LINUX_PLATFORMS += amd64
# LINUX_PLATFORMS += 386
# LINUX_PLATFORMS += s390x
# LINUX_PLATFORMS += ppc64le
# LINUX_PLATFORMS += arm-v6
# LINUX_PLATFORMS += arm-v7
LINUX_PLATFORMS += arm64-v8

WINDOWS_PLATFORMS += nanoserver-1809
WINDOWS_PLATFORMS += nanoserver-2004
WINDOWS_PLATFORMS += nanoserver-20h2
WINDOWS_PLATFORMS += nanoserver-21h1

build-linux:
	@ make $(foreach PLATFORM,$(LINUX_PLATFORMS),platform-linux-$(PLATFORM))

build-windows:
	@ make $(foreach PLATFORM,$(WINDOWS_PLATFORMS),platform-windows-$(PLATFORM))

platform-linux-%:
	@ docker buildx build \
		--platform $(subst -,/,$(subst platform-,,$@)) \
		-t $(CI_REGISTRY_IMAGE):$(VERSION)-$(subst -,,$(subst linux-,,$(subst platform-,,$@)))-$(CI_COMMIT_SHORT_SHA) .

platform-windows-nanoserver-20h2:
	@ docker build -f Dockerfile.windows --build-arg=TAG=20h2 --build-arg=GOLANG_TAG=20H2 \
		-t $(CI_REGISTRY_IMAGE):$(VERSION)-$(subst platform-windows-,,$@)-$(CI_COMMIT_SHORT_SHA) .

platform-windows-nanoserver-21h1:
	@ docker build -f Dockerfile.windows --build-arg=TAG=ltsc2022 --build-arg=GOLANG_TAG=ltsc2022 \
		-t $(CI_REGISTRY_IMAGE):$(VERSION)-$(subst platform-windows-,,$@)-$(CI_COMMIT_SHORT_SHA) .

platform-windows-%:
	@ docker build -f Dockerfile.windows --build-arg=TAG=$(subst platform-windows-nanoserver-,,$@) --build-arg=GOLANG_TAG=$(subst platform-windows-nanoserver-,,$@) \
		-t $(CI_REGISTRY_IMAGE):$(VERSION)-$(subst platform-windows-,,$@)-$(CI_COMMIT_SHORT_SHA) .

push:
	@ docker push ${CI_REGISTRY_IMAGE}

push-multiarch:
	@ docker manifest create --amend ${CI_REGISTRY_IMAGE}:${VERSION} $(foreach PLATFORM,$(LINUX_PLATFORMS),${CI_REGISTRY_IMAGE}:${VERSION}-$(subst -,,$(PLATFORM))-$(CI_COMMIT_SHORT_SHA))
	@ docker manifest create --amend ${CI_REGISTRY_IMAGE}:${VERSION} $(foreach PLATFORM,$(WINDOWS_PLATFORMS),${CI_REGISTRY_IMAGE}:${VERSION}-$(PLATFORM)-$(CI_COMMIT_SHORT_SHA))
	@ docker manifest push --purge ${CI_REGISTRY_IMAGE}:${VERSION}
