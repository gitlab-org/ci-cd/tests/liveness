FROM --platform=$BUILDPLATFORM golang:1.16-alpine AS builder

ARG TARGETOS
ARG TARGETARCH
ENV GOOS=$TARGETOS GOARCH=$TARGETARCH

WORKDIR /go/src/liveness
COPY . .
RUN echo $GOOS $GOARCH
RUN go build -ldflags="-w -s" -o /go/bin/liveness

FROM --platform=$BUILDPLATFORM alpine:3.12.0
COPY --from=builder /go/bin/liveness /go/bin/liveness

ENV PATH "$PATH:/go/bin/"

ENTRYPOINT ["liveness"]
EXPOSE 80/tcp
